#! /bin/bash
## This script is used to build the project.
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user

export PYSPARK_PYTHON=python3
export PYSPARK_DRIVER_PYTHON=python3
export JAVA_HOME=/usr/jdk64/jdk1.8.0_112
export HADOOP_CONF_DIR=/etc/hadoop/conf

# name, executor-memory, executor-cores, num-executors
# and driver-memory needs to be custom/params for each job
cd /tmp
zip packs.zip ${ROOT_FOLDER}
cp /tmp/packs.zip ${ROOT_FOLDER}/packs.zip
cd ${ROOT_FOLDER}

spark-submit --master yarn \
       --py-files packs.zip \
       --name ${PROJECT_NAME} \
       --deploy-mode cluster \
       --executor-memory 3G \
       --executor-cores 3 \
       --num-executors 3 \
       --driver-memory 2G \
       app/main.py
